#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Reviews
Feature: Add a product review
  As a confirmed app user
  I want be able to leave a review
  In order to share my opinion

  @ProductReview
  Scenario Outline: Add a review to the first product
    Given I am on the landing activity
    When I click on the product 2
    And I click on add review button
    And I fill the comment '<comment>', grade '<value>' and save
    And I go back to the previous activity
    And I click on the product 2
    Then a review with comment '<comment>' and grade '<value>' should be displayed

    Examples: 
      | comment			 			    | value |
      | my feet look tiny now |     5 | 