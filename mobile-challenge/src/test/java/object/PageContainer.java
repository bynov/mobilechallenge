package object;

import io.appium.java_client.android.AndroidDriver;
import pages.AddReviewActivity;
import pages.MainActivity;
import pages.ProductActivity;

public class PageContainer {
   
   private AndroidDriver androidDriver;
   private MainActivity mainActivity;
   private AddReviewActivity addReviewActivity;
   private ProductActivity productActivity;

   public PageContainer(AndroidDriver androidDriverParam)
   {
	   this.androidDriver = androidDriverParam;
   }
	
   public AddReviewActivity getAddReviewActivity()
   {
        return (addReviewActivity == null) ? addReviewActivity = new AddReviewActivity(androidDriver) : addReviewActivity;
   }
   
   public MainActivity getMainActivity()
   {
        return (mainActivity == null) ? mainActivity = new MainActivity(androidDriver) : mainActivity;
   }
   
   public ProductActivity getProductActivity()
   {
        return (productActivity == null) ? productActivity = new ProductActivity(androidDriver) : productActivity;
   }

}
