package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import io.appium.java_client.android.AndroidDriver;

public class ProductActivity extends ParentActivity {

	AndroidDriver androidDriver;
	String addReviewButton = "com.example.challenge:id/addReview";
	By reviewView = By.xpath("//androidx.recyclerview.widget.RecyclerView");

	public ProductActivity(AndroidDriver androidDriverParam)
	{
		androidDriver = androidDriverParam;
	}
	
	public void clickAddReviewButton(AndroidDriver driver)
	{
		driver.findElementById(addReviewButton).click();
	}
	
	public Boolean isReviewDisplayed(AndroidDriver driver, String comment, String grade)
	{
		try
		{
			WebElement reviewViewElement = driver.findElement(reviewView);
			new Actions(driver).clickAndHold(reviewViewElement).moveByOffset(0, -10000).perform();
			By lastAddedReview = By.xpath(String.format("//android.widget.TextView[@text='%s']", comment + " " + grade));
			WebElement lastReviewElement =  driver.findElement(lastAddedReview);
			return true;
		}
		catch (NoSuchElementException ex)
		{
			return false;
		}
		
	}

}
