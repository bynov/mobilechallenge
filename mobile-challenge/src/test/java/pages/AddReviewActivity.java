package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;

public class AddReviewActivity extends ParentActivity {

	AndroidDriver androidDriver;
	String textFieldClass = "android.widget.EditText";
	String gradeSpinnerId = "android:id/text1";
	String gradeOption = "//android.widget.CheckedTextView[@text='%s']";
	String saveButtonId = "com.example.challenge:id/saveReview";
	By saveButtonIdBy = By.id(saveButtonId);
	
	public AddReviewActivity(AndroidDriver driver) 
	{	
		this.androidDriver = driver;
	}
	
	public void AddReview(AndroidDriver androidDriver, String comment, String grade)
	{
		androidDriver.findElementByClassName(textFieldClass).sendKeys(comment);
		androidDriver.findElementById(gradeSpinnerId).click();
		WebElement gradeOptionElement = androidDriver.findElementByXPath(String.format(gradeOption, grade));
		Actions action = new Actions(androidDriver);
		action.moveToElement(gradeOptionElement).build().perform();
		gradeOptionElement.click();
		androidDriver.findElement(saveButtonIdBy).click();
	}
	
}
