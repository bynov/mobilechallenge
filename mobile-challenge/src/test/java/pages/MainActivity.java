package pages;

import io.appium.java_client.android.AndroidDriver;

public class MainActivity extends ParentActivity {
	
	AndroidDriver driver;
	String productToClickInOverview = "//android.view.ViewGroup[%s]";

	public MainActivity(AndroidDriver driver) 
	{	
		this.driver = driver;
	}
	
	public void clickProductInOverview(AndroidDriver androidDriver, Integer productNumberInOverview)
	{
		androidDriver.findElementByXPath(String.format(productToClickInOverview, productNumberInOverview)).click();
	}

	
	
}
