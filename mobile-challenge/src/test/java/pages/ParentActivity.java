package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;

public class ParentActivity {

	protected ParentActivity() {
		
	}

	protected void clickElementJs(AndroidDriver driver, By element, int timeout)
	{
		WebElement webElem = driver.findElement(element);
		JavascriptExecutor exec = (JavascriptExecutor)driver;
		try { exec.executeScript("arguments[0].click();", webElem); }
		catch(StaleElementReferenceException ex) 
		{
			webElem = driver.findElement(element);
			exec.executeScript("arguments[0].click();", webElem);
		}
	}
	
	protected void enterText (AndroidDriver driver, By by, String textToEnter)
	{
		driver.findElement(by).sendKeys(textToEnter);
	}

	protected Boolean isThereAnAlert(AndroidDriver driver)
	{
		try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }    
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }
	}
	
	protected void closeAlert(AndroidDriver driver) 
	{
		if(isThereAnAlert(driver))
		{
			driver.switchTo().alert().dismiss();
		}
	}
	
	public void ScrollToElement(AndroidDriver driver, WebElement element)
	{
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

}
