package cucumber;

import object.PageContainer;
import setup.Capabilities;

public class TestContext {
	
     private ScenarioContext scenarioContext;
     private Capabilities capabilities;
     private PageContainer pageContainer;

	 public TestContext() throws Exception{
		 scenarioContext = new ScenarioContext();
		 capabilities = new Capabilities();
		 pageContainer = new PageContainer(capabilities.getDriver());
	 }
	 
	 public PageContainer getPageContainer()
	 {
		 return this.pageContainer;
	 }
	 
	 public Capabilities getCapabilities()
	 {
		 return this.capabilities;
	 }
	 
	 public ScenarioContext getScenarioContext() {
		 return scenarioContext;
	 }
 
}
