package setup;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Capabilities extends DesiredCapabilities {
	
    protected AndroidDriver androidDriver;
    private AppiumDriverLocalService service;
    
    public void preparation() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("deviceName", "08271FDD40017R");
        capabilities.setCapability("appPackage", "com.example.challenge");
        capabilities.setCapability("appActivity", "com.example.challenge.MainActivity");
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
        String service_url = service.getUrl().toString();
        System.out.println("Appium Service Address: " + service_url);
        androidDriver = new AndroidDriver
        	(new URL(service_url), capabilities);
        androidDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    } 
    
    public AndroidDriver getDriver() {
    	return this.androidDriver;
    }
    
	public void stopServer() {
	    service.stop();
	}
}