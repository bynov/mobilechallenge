package testStepsDefs;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import cucumber.TestContext;
import enums.Context;
import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.AddReviewActivity;
import pages.MainActivity;
import pages.ProductActivity;

public class AddProductReviewStepsDefs {

	TestContext testContext;
	AddReviewActivity addReviewActivity;
	ProductActivity productActivity;
	MainActivity mainActivity;
	
	
	public AddProductReviewStepsDefs(TestContext context) {
		testContext = context;
		addReviewActivity = testContext.getPageContainer().getAddReviewActivity();
		productActivity = testContext.getPageContainer().getProductActivity();
		mainActivity = testContext.getPageContainer().getMainActivity();
	}
	
	@Before
	public void beforeScenario() throws Exception {
		System.out.println("-------------- Start of Scenario --------------");
		testContext.getCapabilities().preparation();
		//start server
	}

	@After
	public void afterScenario() throws Exception {
		System.out.println("-------------- End of Scenario --------------");
		testContext.getCapabilities().stopServer();
		//stop server
		
	}

	@Given("I am on the landing activity")
	public void i_am_on_the_landing_activity() {
		System.out.println("-------------- App launched --------------");
	}
	
	@When("I click on the product {int}")
	public void i_click_on_the_product(Integer productNumberInOverview) {
		mainActivity.clickProductInOverview(testContext.getCapabilities().getDriver() , productNumberInOverview);
	}
	@When("I click on add review button")
	public void i_click_on_add_review_button() {
		productActivity.clickAddReviewButton(testContext.getCapabilities().getDriver());
	}
	
	@When("I fill the comment {string}, grade {string} and save")
	public void i_fill_the_comment_value_and_save(String comment, String grade) {
		addReviewActivity.AddReview(testContext.getCapabilities().getDriver(), comment, grade);
		testContext.getScenarioContext().setContext(Context.PRODUCT_COMMENT, comment);
		testContext.getScenarioContext().setContext(Context.PRODUCT_GRADE, grade);
	}
	@When("I go back to the previous activity")
	public void i_go_back_to_the_previous_activity() {
		testContext.getCapabilities().getDriver().navigate().back();
	}
	
	@Then("a review with comment {string} and grade {string} should be displayed")
	public void a_review_with_comment_and_value_should_be_displayed(String comment, String grade) {
		Assert.assertTrue(productActivity.isReviewDisplayed(testContext.getCapabilities().getDriver(),
				testContext.getScenarioContext().getContext(Context.PRODUCT_COMMENT).toString(),
				testContext.getScenarioContext().getContext(Context.PRODUCT_GRADE).toString()));
	}
	

}
