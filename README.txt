Instructions to run the tests on a plugged device

- An android smartphone with the apk (provided by Adidas) installed
	- with developer options enabled (click 7 times on "Build version" in your phone settings -> about the phone)
- Java (especially jdk), node, appium (npm package), android sdk up to date 
- Eclipse up to date with maven and cucumber plugins 
- Plug it to your laptop and allow USB debugging in developers options on your phone 
	- In your android folder -> platform-tools folder, open a command prompt and run "adb devices"
	- In Capabilities.java class, replace the "deviceName" value (currently my phone value is set) with the value from the command (column List Of Devices)
- After cloning the repository, import in eclipse as a maven project
	-Right-click on the class "AddProductReviewRunner" (in src/test/java/testStepsRunner) -> Run as -> JUnit class
	
